package chess;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

/**
 * Describes a tile on the chess board.
 *
 * @author Jotham Matu <.com>
 */
public class Tile extends java.awt.Rectangle
{

    /**
     * Specifies whether a tile is white or black.
     */
    public static enum TILE_TYPE
    {

	WHITE, BLACK;

	/**
	 * Changes from WHITE to BLACK, or BLACK to WHITE.
	 *
	 * @param type the TILE_TYPE to toggle.
	 * @return the new value.
	 */
	TILE_TYPE toggle(TILE_TYPE type)
	{
	    if (type == Tile.TILE_TYPE.WHITE)
	    {
		type = TILE_TYPE.BLACK;
	    } else
	    {
		type = Tile.TILE_TYPE.WHITE;
	    }
	    return type;
	}
    };

    /**
     * Size of all the tiles on the board.
     */
    public static Dimension SIZE = new Dimension(
	    (int) (Board.WIDTH / 8),
	    (int) (Board.HEIGHT / 8));
    /**
     * This tile's color.
     */
    private Color myColor;

    /**
     * Creates a new tile.
     *
     * @param location it's position on the board.
     * @param type its color.
     */
    public Tile(Point location, TILE_TYPE type)
    {
	super(location, SIZE);

	if (type == TILE_TYPE.WHITE)
	{
	    myColor = Color.WHITE;
	} else
	{
	    myColor = Color.GRAY;
	}
    }

    /**
     * Get's the color of this Tile.
     *
     * @return either white or black.
     */
    public Color getColor()
    {
	return myColor;
    }
}
