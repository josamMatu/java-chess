package chess;

import chess.pieces.Bishop;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Pawn;
import chess.pieces.Piece;
import chess.pieces.Queen;
import chess.pieces.Rook;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * The chess board.
 *
 * @author Jotham Matu <jotham@itgrowth.net>
 */
public class Board extends java.awt.Canvas implements java.lang.Runnable
{

    public static final long serialVersionUID = 1L;
    /**
     * Scale used to render.
     */
    public static final float SCALE = 80;
    /**
     * Width of the chess board.
     */
    public static final float WIDTH = 8 * SCALE;
    /**
     * Height of the chess board.
     */
    public static final float HEIGHT = 8 * SCALE;
    /**
     * Whether the game is running or not.
     */
    private boolean running;
    /**
     * This game's thread.
     */
    private Thread gameThread;
    /**
     * The tiles on the board.
     */
    private Tile[] boardTiles = new Tile[64];
    /**
     * All the game's pieces.
     */
    public static List<Piece> pieces;
    /**
     * The index of the piece the user is moving.
     */
    private int selectedPieceIndex;
    /**
     * Whether or not a piece has been clicked by the user.
     */
    private boolean selectedPiece;
    /**
     * Holds a copy of a piece that a user has selected.<br / >
     *
     * This is what is moved around instead of teh actual piece, so as to cause
     * it to overlap every other piece.
     */
    private Piece movingPiece;
    /**
     * The distance from the top-left corner of the selected piece the mouse is.
     */
    private Dimension movementOffset;
    /**
     * The location of a piece before it was moved.
     */
    private Point originalLocation;
    /**
     * Holds whether or not an illegal move has been made.
     */
    private boolean illegalMove;
    /**
     * The time since a message was shown.
     */
    private byte messageTime;

    public Board()
    {
	// Setting the dimensions
	setSize((int) WIDTH, (int) HEIGHT);

	// Getting the board's tiles
	getTiles();

	// Getting the game's pieces
	getPieces();
	selectedPieceIndex = -99; // Symbolic of 'no piece selected'
	movingPiece = null;

	// Setting the event listeners
	setListeners();

	illegalMove = false;
	messageTime = 0;
    }

    /**
     * Creates and positions all the tiles on the board.
     */
    private void getTiles()
    {
	int x = 0;
	int y = 0;
	Tile.TILE_TYPE type = Tile.TILE_TYPE.WHITE;

	for (int tile = 0; tile < boardTiles.length; tile++)
	{
	    boardTiles[tile] = new Tile(new Point(x, y), type);

	    // Moving to the right
	    x += Tile.SIZE.width;
	    // If this was the right-most tile, move to the next line
	    if (x == WIDTH)
	    {
		x = 0;
		y += Tile.SIZE.height;

		// Toggling the color here, to create a checkered effect
		type = type.toggle(type);
	    }
	    // Toggling the color
	    type = type.toggle(type);
	}
    }

    /**
     * Gets the game's pieces.
     */
    private void getPieces()
    {
	// Initializing (1st play) / deleting everything (restarts)
	pieces = new ArrayList<>();

	/*int xMargin = (int) (Tile.SIZE.width * 0.1);
	 int yMargin = (int) (Tile.SIZE.height * 0.1);*/

	// <editor-fold desc="Creating black pieces" defaultstate="collapsed">
	// pawns
	int y = (int) ((HEIGHT / 8) * 1);
	int x = 0;
	for (int pawn = 0; pawn < 8; pawn++)
	{
	    pieces.add(new Pawn(new Point(x, y), Piece.PIECE_TYPE.BLACK));

	    x += Tile.SIZE.width;
	}
	// king
	y = 0;
	x = (int) ((WIDTH / 8) * 3);
	pieces.add(new King(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	// queen
	x = (int) ((WIDTH / 8) * 4);
	pieces.add(new Queen(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	// bishops
	x = (int) ((WIDTH / 8) * 2);
	pieces.add(new Bishop(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	x = (int) ((WIDTH / 8) * 5);
	pieces.add(new Bishop(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	// knights
	x = (int) ((WIDTH / 8) * 1);
	pieces.add(new Knight(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	x = (int) ((WIDTH / 8) * 6);
	pieces.add(new Knight(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	// rooks
	x = (int) ((WIDTH / 8) * 0);
	pieces.add(new Rook(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	x = (int) ((WIDTH / 8) * 7);
	pieces.add(new Rook(new Point(x, y), Piece.PIECE_TYPE.BLACK));
	// </editor-fold>

	// <editor-fold desc="Creating white pieces" defaultstate="collapsed">
	// pawns
	y = (int) ((HEIGHT / 8) * 6);
	x = 0;
	for (int pawn = 0; pawn < 8; pawn++)
	{
	    pieces.add(new Pawn(new Point(x, y), Piece.PIECE_TYPE.WHITE));

	    x += Tile.SIZE.width;
	}
	// king
	y = (int) ((HEIGHT / 8) * 7);
	x = (int) ((WIDTH / 8) * 3);
	pieces.add(new King(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	// queen
	x = (int) ((WIDTH / 8) * 4);
	pieces.add(new Queen(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	// bishops
	x = (int) ((WIDTH / 8) * 2);
	pieces.add(new Bishop(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	x = (int) ((WIDTH / 8) * 5);
	pieces.add(new Bishop(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	// knights
	x = (int) ((WIDTH / 8) * 1);
	pieces.add(new Knight(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	x = (int) ((WIDTH / 8) * 6);
	pieces.add(new Knight(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	// rooks
	x = (int) ((WIDTH / 8) * 0);
	pieces.add(new Rook(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	x = (int) ((WIDTH / 8) * 7);
	pieces.add(new Rook(new Point(x, y), Piece.PIECE_TYPE.WHITE));
	// </editor-fold>
    }

    private void setListeners()
    {
	this.addMouseMotionListener(new MouseAdapter()
	{
	    // For checking hovers
	    @Override
	    public void mouseMoved(MouseEvent e)
	    {
		Board.this.mouseMoved(e);
	    }

	    // For allowing pieces to be moved
	    @Override
	    public void mouseDragged(MouseEvent e)
	    {
		Board.this.mouseDragged(e);
	    }
	});

	this.addMouseListener(new MouseAdapter()
	{
	    // For allowing pieces to be selected by the user
	    @Override
	    public void mousePressed(MouseEvent e)
	    {
		Board.this.mousePressed(e);
	    }

	    // For releasing user-selected pieces
	    @Override
	    public void mouseReleased(MouseEvent e)
	    {
		Board.this.mouseReleased(e);
	    }
	});
    }

    /**
     * Checks if the mouse is hovering over a chess piece. If it is, it changes
     * the cursor to a hand.
     *
     * @param e
     */
    private void mouseMoved(MouseEvent e)
    {
	// If no piece is currently selected for movement
	if (!selectedPiece)
	{
	    boolean hovered = false; // Whether a piece was hovered over

	    for (int piece = 0; piece < pieces.size(); piece++)
	    {
		if (pieces.get(piece).contains(e.getPoint()))
		{
		    this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		    hovered = true;
		    // Setting the selected piece here so that this loop will not
		    // be repeated in mousePressed. It's just saving some time.
		    selectedPieceIndex = piece;
		    break;
		}
	    }
	    if (!hovered)
	    {
		this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		selectedPieceIndex = -99;
	    }
	}
    }

    /**
     * For when the mouse is pressed on a chess piece.<br />
     *
     * It creates a copy of this piece that can then be moved around.
     *
     * @param e
     */
    private void mousePressed(MouseEvent e)
    {
	if (selectedPieceIndex != -99)
	{
	    // Copying this piece.
	    movingPiece = pieces.get(selectedPieceIndex);
	    originalLocation = movingPiece.getLocation();

	    // Noting how far the cursor is from the top-left corner of this
	    // piece. This' for use in rendering its movement correctly.
	    movementOffset = new Dimension(
		    e.getX() - movingPiece.x,
		    e.getY() - movingPiece.y);

	    selectedPiece = true;
	}
    }

    /**
     * For when a chess-piece is being moved.
     *
     * @param e
     */
    private void mouseDragged(MouseEvent e)
    {
	// If a chess-piece was selected
	if (selectedPiece)
	{
	    movingPiece.setLocation(
		    e.getX() - movementOffset.width,
		    e.getY() - movementOffset.height);
	}
    }

    /**
     * For when a user-selected piece is released.
     *
     * @param e
     */
    private void mouseReleased(MouseEvent e)
    {
	// If a chess-piece was selected
	if (selectedPiece)
	{
	    // If the user made a legal move
	    if (this.checkIfLegal(e))
	    {
		// If this was a kill move, kill whatever he/she choose to kill
		for (Integer index : movingPiece.getEatablePieces())
		{
		    if (movingPiece.intersects(pieces.get(index)))
		    {
			// Checking if a king has been killed
			if (this.checkIfKingWasKilled(index))
			{
			    // Avoiding pieces.remove() being called after the game restarts
			    break;
			}
			pieces.remove(index.intValue());
			break;
		    }
		}
	    } // If the user made an illegal move
	    else
	    {
		movingPiece.setLocation(originalLocation);
		// Allow the "illegal move" message to be shown
		illegalMove = true;
		messageTime = 0;
	    }

	    // Clearing the movement stuff
	    movingPiece.resetPossibleMovement();
	    movingPiece = null;
	    selectedPiece = false;
	    selectedPieceIndex = -99;
	}
    }

    /**
     * Finds out if the user made a legal move.
     *
     * @return true if it is legal and false otherwise.
     */
    private boolean checkIfLegal(MouseEvent e)
    {
	List<Point> legalMoves = movingPiece.nextMove(originalLocation);

	for (Point p : legalMoves)
	{
	    if (new Rectangle(p, movingPiece.getSize()).contains(e.getPoint()))
	    {
		movingPiece.setLocation(p);
		return true;
	    }
	}

	return false;
    }

    /**
     * Checks if the king was eaten.
     *
     * @param index the index of the most recently eaten piece in * *
     * the <code>pieces</code> list
     * @return true if it has been eaten and false otherwise
     */
    private boolean checkIfKingWasKilled(int index)
    {
	// Finding out the class of that piece
	if (pieces.get(index).getClass().equals(King.class))
	{
	    // Finding out the color
	    Piece.PIECE_TYPE color = pieces.get(index).getType();

	    // Making it disappear (For effect)
	    pieces.remove(index);

	    // If it was the white king
	    if (color == Piece.PIECE_TYPE.WHITE)
	    {
		endGame("black");
	    } // If it was the black king
	    else
	    {
		endGame("white");
	    }
	    return true;
	}
	return false;
    }

    /**
     * For when a king has been killed.
     *
     * @param winner the player that won.
     */
    private void endGame(String winner)
    {
	int response = JOptionPane.showConfirmDialog(
		this,
		"The " + winner + "player has won.\nWould you like to restart?",
		"Game Won",
		JOptionPane.YES_NO_OPTION);

	if (response == JOptionPane.OK_OPTION)
	{
	    this.getPieces();
	} else
	{
	    this.running = false;
	}
    }

    /**
     * Starts this Thread (the game).
     */
    public synchronized void start()
    {
	gameThread = new Thread(this, "Game Thread");
	running = true;
	gameThread.start();
    }

    /**
     * This Thread's process (the <u>game loop</u>).
     */
    @Override
    public void run()
    {
	while (running)
	{
	    try
	    {
		Thread.sleep(1000 / 60);
	    } catch (Exception e)
	    {
	    }
	    render();
	}
    }

    private void render()
    {
	// Proposing triple buffering
	BufferStrategy buffStrategy = getBufferStrategy();
	if (buffStrategy == null)
	{
	    createBufferStrategy(3);
	    return;
	}

	Graphics graphics = buffStrategy.getDrawGraphics();

	// Drawing the background
	this.drawTiles(graphics);

	// Drawing the pieces
	this.drawPieces(graphics);

	// Drawing any piece that should be moving
	this.drawMovingPiece(graphics);

	// Drawing any information messages
	/// Since rendering is done 16.67 times a second, showing a message for
	/// 1 second is calling this.drawMessage around 16 times.
	if (illegalMove && messageTime < 20)
	{
	    this.drawMessage(graphics);
	    messageTime++;
	} else
	{
	    illegalMove = false;
	}

	// Showing the rendering
	graphics.dispose();
	buffStrategy.show();
    }

    /**
     * Draws all the board's tiles.
     *
     * @param graphics the object to use to draw.
     */
    private void drawTiles(Graphics graphics)
    {
	for (Tile tile : boardTiles)
	{
	    graphics.setColor(tile.getColor());
	    graphics.fillRect(tile.x, tile.y, tile.width, tile.height);
	}
    }

    /**
     * Draws the game's pieces.
     *
     * @param graphics
     */
    private void drawPieces(Graphics graphics)
    {
	for (int piece = 0; piece < pieces.size(); piece++)
	{
	    // Making sure this isn't a selected piece.
	    // A selected piece isn't drawn. A copy of it is.
	    if (!(selectedPiece && selectedPieceIndex == piece))
	    {
		Piece p = pieces.get(piece);

		graphics.drawImage(p.getImage(), p.x, p.y, p.width, p.height, this);
	    }
	}
    }

    /**
     * Draws any piece that the user is moving.
     *
     * @param graphics
     */
    private void drawMovingPiece(Graphics graphics)
    {
	// If there is a piece to be moved
	if (selectedPiece)
	{
	    // A bounding rectangle for easing placement by the user
	    graphics.setColor(Color.CYAN);
	    graphics.drawRect(
		    movingPiece.x, movingPiece.y,
		    movingPiece.width, movingPiece.height);

	    // The piece
	    graphics.drawImage(
		    movingPiece.getImage(),
		    movingPiece.x, movingPiece.y,
		    movingPiece.width, movingPiece.height,
		    this);
	}
    }

    private void drawMessage(Graphics graphics)
    {
	Rectangle messageArea = new Rectangle(
		(int) (WIDTH * 0.7), 0,
		(int) (WIDTH * 0.3), (int) (HEIGHT * 0.1));

	graphics.setColor(Color.MAGENTA);
	graphics.fillRoundRect(
		messageArea.x, messageArea.y,
		messageArea.width, messageArea.height,
		(int) (messageArea.width * 0.1), (int) (messageArea.height * 0.1));

	graphics.setColor(Color.white);
	graphics.drawString(
		"Illegal move.",
		messageArea.x + (int) (messageArea.width * 0.25),
		messageArea.y + (int) (messageArea.height * 0.5));
    }
}
