package chess.pieces;

import chess.Board;
import java.awt.Point;
import java.util.List;

/**
 * Describes a knight.
 *
 * @author Jotham Matu <.com>
 */
public class Knight extends Piece
{

    /**
     * Creates a new knight.
     *
     * @param location it's position on the board canvas.
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    public Knight(Point location, Piece.PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, type);

	// Getting the associated image
	this.setImage(type);
    }

    /**
     * Gets the appropriate image.
     *
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    final void setImage(Piece.PIECE_TYPE type)
    {
	if (type == Piece.PIECE_TYPE.WHITE)
	{
	    super.setImage("whiteknight");
	} else
	{
	    super.setImage("blackknight");
	}
    }

    /**
     * Gets all the legal moves one could make.
     *
     * @param originalLocation this piece's original location.
     * @return list with the top-left corners of the Tiles that can be moved to.
     */
    @Override
    public List<Point> nextMove(Point originalLocation)
    {
	int moveXBy = (int) (this.getHeight());
	int moveYBy = (int) (this.getHeight());

	// <editor-fold desc="Down" defaultstate="collapsed">
	int currY = originalLocation.y + (moveYBy * 2);
	int currX = originalLocation.x + moveXBy;
	if (currX < Board.WIDTH && currY < Board.HEIGHT)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	currX = originalLocation.x - moveXBy;
	if (currX >= 0 && currY < Board.HEIGHT)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>

	// <editor-fold desc="Up" defaultstate="collapsed">
	currY = originalLocation.y - (moveYBy * 2);
	currX = originalLocation.x - moveXBy;
	if (currX >= 0 && currY >= 0)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	currX = originalLocation.x + moveXBy;
	if (currX < Board.WIDTH && currY >= 0)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>

	// <editor-fold desc="Left" defaultstate="collapsed">
	currX = originalLocation.x - (moveXBy * 2);
	currY = originalLocation.y - moveYBy;
	if (currX >= 0 && currY >= 0)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	currY = originalLocation.y + moveXBy;
	if (currX >= 0 && currY < Board.HEIGHT)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>

	// <editor-fold desc="Right" defaultstate="collapsed">
	currX = originalLocation.x + (moveXBy * 2);
	currY = originalLocation.y - moveYBy;
	if (currX < Board.WIDTH && currY >= 0)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	currY = originalLocation.y + moveXBy;
	if (currX < Board.WIDTH && currY < Board.HEIGHT)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>

	return possibleLocations;
    }
}
