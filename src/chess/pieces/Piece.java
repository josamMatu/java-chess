package chess.pieces;

import chess.Board;
import chess.Tile;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

/**
 * A chess piece.
 *
 * @author jotham
 */
public class Piece extends Rectangle
{

    /**
     * Whether a piece is black or white.
     */
    public static enum PIECE_TYPE
    {

	WHITE, BLACK
    };

    /**
     * Whether a move made is valid, a kill move or illegal.<br /> This is
     * especially useful in
     * <code>Piece.checkIfExists(Point point)</code>
     */
    public static enum MOVE_TYPE
    {

	OK, EAT, WRONG
    };
    /**
     * The image to be associated with this piece.
     */
    private Image image;
    /**
     * Whether it's black or white.<br /> This has significance what a piece can
     * <b>kill</b> and on <b>pawn direction</b>.
     */
    protected PIECE_TYPE type;
    /**
     * The size of a chess piece.
     */
    public static Dimension SIZE = new Dimension(
	    (int) (Tile.SIZE.width/* * 0.8*/),
	    (int) (Tile.SIZE.height/* * 0.8*/));
    /**
     * Holds all the moves this piece can legally make.
     */
    protected List<Point> possibleLocations;
    /**
     * Holds the indexes of all the pieces in
     * <code>Board.pieces</code> that this piece can eat at a particular time.
     * It should be reset after each move by
     * <code>this.resetPossibleMovement</code>.
     */
    protected List<Integer> eatablePieces;

    /**
     * Creates a new chess piece.
     */
    public Piece(Point location, PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, Piece.SIZE);
	this.type = type;

	possibleLocations = new ArrayList<>();
	eatablePieces = new ArrayList<>();
    }

    public void setImage(String filename)
    {
	try
	{
	    image = ImageIO.read(Piece.class.getResource("/chess/images/" + filename + ".png"));

	} catch (Exception e)
	{
	    image = null;
	}
    }

    public Image getImage()
    {
	return image;
    }

    public PIECE_TYPE getType()
    {
	return type;
    }

    /**
     * Should be <b>overridden.</b>
     *
     * @param originalLocation this piece's original location.
     *
     * @return A list of the top-left corners of the Tiles that can be moved to.
     */
    public List<Point> nextMove(Point originalLocation)
    {
	throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Checks if any piece is at the location where this piece could move to.<br
     * /> <ul> <li>If it is, it finds out if it can eat whatever is there.
     * <ul><li>If it can adds it to
     * <code>this.possibleLocations</code> and
     * <code>this.eatablePieces</code></li></ul>Then returns
     * <code>true</code></li> <li>If it isn't it adds it to
     * <code>this.possibleLocations</code> and returns
     * <code>false</code></li></ul>
     *
     * @param point the location to check at
     * @return true on finding hindrance and false otherwise.
     */
    public boolean checkIfExists(Point point)
    {
	for (int index = 0; index < Board.pieces.size(); index++)
	{
	    Piece piece = Board.pieces.get(index);

	    // If the point is within a piece on the board
	    if (piece.contains(point))
	    {
		// If the piece is of a different color
		if (piece.type != this.type)
		{
		    this.possibleLocations.add(point);
		    // Noting that it's eatable
		    this.eatablePieces.add(index);
		}
		return true;
	    }
	}
	// If there's no hindrance to this piece
	this.possibleLocations.add(point);
	return false;
    }

    /**
     * Removes all the locations this piece is likely to move to from
     * <code>possibleLocations List</code> and all the indexes currently in the
     * <code>eatablePieces List</code>.
     */
    public void resetPossibleMovement()
    {
	this.possibleLocations.clear();
	this.eatablePieces.clear();
    }

    /**
     * Gets all the pieces this piece can eat.
     *
     * @return a list of <code>Board.pieces</code> indexes.
     */
    public List<Integer> getEatablePieces()
    {
	return this.eatablePieces;
    }
}
