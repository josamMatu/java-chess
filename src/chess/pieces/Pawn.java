package chess.pieces;

import chess.Board;
import java.awt.Point;
import java.util.List;

/**
 * Describes a pawn.
 *
 * @author Jotham Matu <.com>
 */
public class Pawn extends Piece
{

    /**
     * Creates a new pawn.
     *
     * @param location it's position on the board canvas.
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    public Pawn(Point location, PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, type);

	// Getting the associated image
	this.setImage(type);
    }

    /**
     * Gets the appropriate image.
     *
     * @param type
     */
    final void setImage(PIECE_TYPE type)
    {
	if (type == PIECE_TYPE.WHITE)
	{
	    super.setImage("whitepawn");
	} else
	{
	    super.setImage("blackpawn");
	}
    }

    /**
     * Gets all the legal moves one could make.
     *
     * @param originalLocation this piece's original location.
     * @return list with the top-left corners of the Tiles that can be moved to.
     */
    @Override
    public List<Point> nextMove(Point originalLocation)
    {
	int moveYBy;
	// If this is white, it means it can only go up
	if (getType() == PIECE_TYPE.WHITE)
	{
	    moveYBy = -(int) (this.getHeight());
	} // If it's black, it only goes down
	else
	{
	    moveYBy = (int) (this.getHeight());
	}

	// <editor-fold desc="Moving Forward" defaultstate="collapsed">

	int newY = originalLocation.y + moveYBy;
	if (newY >= 0 && newY < Board.HEIGHT) // Ensuring the point is within the board
	{
	    Point p = new Point(originalLocation.x, newY);

	    // If there isn't a piece directly infront of it.
	    checkIfExists(p);
	}
	// </editor-fold>

	// <editor-fold desc="Eating Opponent's Pieces" defaultstate="collapsed">
	int moveXBy = (int) (this.getWidth());

	int newX = originalLocation.x + moveXBy; // Moving to the right
	if ((newY >= 0 && newY < Board.HEIGHT) && newX < Board.WIDTH)
	{
	    Point p  = new Point(newX, newY);
	    // Finding out if it can be eaten
	    checkIfEatable(p);
	}
	newX = originalLocation.x - moveXBy; // Moving to the left
	if ((newY >= 0 && newY < Board.HEIGHT) && newX >= 0)
	{
	    Point p  = new Point(newX, newY);
	    // Finding out if it can be eaten
	    checkIfEatable(p);
	}
	// </editor-fold>

	return possibleLocations;
    }

    /**
     * Created just for Pawns coz they eat diagonally instead of vertically or
     * laterally.
     *
     * @param p the point to check for an eatable piece at.
     * @return true if it found an eatable piece and false otherwise
     */
    @Override
    public boolean checkIfExists(Point point)
    {
	for (int index = 0; index < Board.pieces.size(); index++)
	{
	    Piece piece = Board.pieces.get(index);

	    // If the point is within a piece on the board
	    if (piece.contains(point))
	    {
		return true;
	    }
	}
	// If there's no hindrance to this piece
	this.possibleLocations.add(point);
	return false;
    }

    /**
     * Finds out if any piece falls within a certain point so that this pawn
     * could eat it.
     *
     * @param point the location to check.
     */
    private void checkIfEatable(Point point)
    {
	for (int index = 0; index < Board.pieces.size(); index++)
	{
	    Piece piece = Board.pieces.get(index);

	    // If the point is within a piece on the board
	    if (piece.contains(point))
	    {
		// If the piece is of a different color
		if (piece.type != this.type)
		{
		    this.possibleLocations.add(point);
		    // Noting that it's eatable
		    this.eatablePieces.add(index);
		}
	    }
	}
    }
}
