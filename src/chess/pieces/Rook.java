package chess.pieces;

import chess.Board;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes a rook.
 *
 * @author Jotham Matu <.com>
 */
public class Rook extends Piece
{

    /**
     * Creates a new rook.
     *
     * @param location it's position on the board canvas.
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    public Rook(Point location, Piece.PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, type);

	// Getting the associated image
	this.setImage(type);
    }

    /**
     * Gets the appropriate image.
     *
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    final void setImage(Piece.PIECE_TYPE type)
    {
	if (type == Piece.PIECE_TYPE.WHITE)
	{
	    super.setImage("whiterook");
	} else
	{
	    super.setImage("blackrook");
	}
    }

    /**
     * Gets all the legal moves one could make.
     *
     * @param originalLocation this piece's original location.
     * @return list with the top-left corners of the Tiles that can be moved to.
     */
    @Override
    public List<Point> nextMove(Point originalLocation)
    {
	int moveXBy = (int) (this.getHeight());
	int moveYBy = (int) (this.getHeight());

	// <editor-fold desc="Vertical locations" defaultstate="collapsed">
	for (int curr = originalLocation.y; curr >= 0; curr -= moveYBy)
	{
	    Point p = new Point(originalLocation.x, curr);
	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	for (int curr = originalLocation.y; curr < Board.HEIGHT; curr += moveYBy)
	{
	    Point p = new Point(originalLocation.x, curr);
	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>

	// <editor-fold desc="Horizontal locations" defaultstate="collapsed">
	for (int curr = originalLocation.x; curr >= 0; curr -= moveXBy)
	{
	    Point p = new Point(curr, originalLocation.y);
	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	for (int curr = originalLocation.x; curr < Board.WIDTH; curr += moveXBy)
	{
	    Point p = new Point(curr, originalLocation.y);
	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>

	return possibleLocations;
    }
}