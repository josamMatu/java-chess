package chess.pieces;

import chess.Board;
import java.awt.Point;
import java.util.List;

/**
 * Describes a queen.
 *
 * @author Jotham Matu <.com>
 */
public class Queen extends Piece
{

    /**
     * Creates a new queen.
     *
     * @param location it's position on the board canvas.
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    public Queen(Point location, Piece.PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, type);

	// Getting the associated image
	this.setImage(type);
    }

    /**
     * Gets the appropriate image.
     *
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    final void setImage(Piece.PIECE_TYPE type)
    {
	if (type == Piece.PIECE_TYPE.WHITE)
	{
	    super.setImage("whitequeen");
	} else
	{
	    super.setImage("blackqueen");
	}
    }

    /**
     * Gets all the legal moves one could make.
     *
     * @param originalLocation this piece's original location.
     * @return list with the top-left corners of the Tiles that can be moved to.
     */
    @Override
    public List<Point> nextMove(Point originalLocation)
    {
	int moveXBy = (int) (this.getWidth());
	int moveYBy = (int) (this.getHeight());
	int currX, currY;

	// <editor-fold desc="To Top-Left" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX >= 0 && currY >= 0;
		currX -= moveXBy, currY -= moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="To Top-Right" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX < Board.WIDTH && currY >= 0;
		currX += moveXBy, currY -= moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="To Bottom-Right" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX < Board.WIDTH && currY < Board.HEIGHT;
		currX += moveXBy, currY += moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="To Bottom-Left" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX >= 0 && currY < Board.HEIGHT;
		currX -= moveXBy, currY += moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="Vertical locations" defaultstate="collapsed">
	for (int curr = originalLocation.y; curr >= 0; curr -= moveYBy)
	{
	    Point p = new Point(originalLocation.x, curr);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	for (int curr = originalLocation.y; curr < Board.HEIGHT; curr += moveYBy)
	{
	    Point p = new Point(originalLocation.x, curr);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="Horizontal locations" defaultstate="collapsed">
	for (int curr = originalLocation.x; curr >= 0; curr -= moveXBy)
	{
	    Point p = new Point(curr, originalLocation.y);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	for (int curr = originalLocation.x; curr < Board.WIDTH; curr += moveXBy)
	{
	    Point p = new Point(curr, originalLocation.y);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	return possibleLocations;
    }
}
