package chess.pieces;

import chess.Board;
import java.awt.Point;
import java.util.List;

/**
 * Describes a bishop.
 *
 * @author Jotham Matu <.com>
 */
public class Bishop extends Piece
{

    /**
     * Creates a new bishop.
     *
     * @param location it's position on the board canvas.
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    public Bishop(Point location, Piece.PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, type);

	// Getting the associated image
	this.setImage(type);
    }

    /**
     * Gets the appropriate image.
     *
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    final void setImage(Piece.PIECE_TYPE type)
    {
	if (type == Piece.PIECE_TYPE.WHITE)
	{
	    super.setImage("whitebishop");
	} else
	{
	    super.setImage("blackbishop");
	}
    }

    /**
     * Gets all the legal moves one could make.
     *
     * @param originalLocation this piece's original location.
     * @return list with the top-left corners of the Tiles that can be moved to.
     */
    @Override
    public List<Point> nextMove(Point originalLocation)
    {
	int moveXBy = (int) (this.getHeight());
	int moveYBy = (int) (this.getHeight());
	int currX, currY;

	// <editor-fold desc="To Top-Left" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX >= 0 && currY >= 0;
		currX -= moveXBy, currY -= moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="To Top-Right" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX < Board.WIDTH && currY >= 0;
		currX += moveXBy, currY -= moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="To Bottom-Right" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX < Board.WIDTH && currY < Board.HEIGHT;
		currX += moveXBy, currY += moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	// <editor-fold desc="To Bottom-Left" defaultstate="collapsed">
	for (currX = originalLocation.x, currY = originalLocation.y;
		currX >= 0 && currY < Board.HEIGHT;
		currX -= moveXBy, currY += moveYBy)
	{
	    Point p = new Point(currX, currY);

	    if (checkIfExists(p))
	    {
		break;
	    }
	}
	// </editor-fold>
	return possibleLocations;
    }
}