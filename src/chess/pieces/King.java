package chess.pieces;

import chess.Board;
import java.awt.Point;
import java.util.List;

/**
 * Describes a king.
 *
 * @author Jotham Matu <.com>
 */
public class King extends Piece
{

    /**
     * Creates a new king.
     *
     * @param location it's position on the board canvas.
     * @param type either PIECE_TYPE.WHITE or PIECE_TYPE.BLACK
     */
    public King(Point location, Piece.PIECE_TYPE type)
    {
	// It will have the same size as a tile
	super(location, type);

	// Getting the associated image
	this.setImage(type);
    }

    /**
     * Gets the appropriate image.
     *
     * @param type
     */
    final void setImage(Piece.PIECE_TYPE type)
    {
	if (type == Piece.PIECE_TYPE.WHITE)
	{
	    super.setImage("whiteking");
	} else
	{
	    super.setImage("blackking");
	}
    }

    /**
     * Gets all the legal moves one could make.
     *
     * @param originalLocation this piece's original location.
     * @return list with the top-left corners of the Tiles that can be moved to.
     */
    @Override
    public List<Point> nextMove(Point originalLocation)
    {
	int moveXBy = (int) (this.getWidth());
	int moveYBy = (int) (this.getHeight());
	int currX, currY;

	// <editor-fold desc="To Top-Left" defaultstate="collapsed">
	currX = originalLocation.x - moveXBy;
	currY = originalLocation.y - moveYBy;
	if (currX >= 0 && currY >= 0)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>
	// <editor-fold desc="To Top-Right" defaultstate="collapsed">
	currX = originalLocation.x + moveXBy;
	currY = originalLocation.y - moveYBy;
	if (currX < Board.WIDTH && currY >= 0)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>
	// <editor-fold desc="To Bottom-Right" defaultstate="collapsed">
	currX = originalLocation.x + moveXBy;
	currY = originalLocation.y + moveYBy;
	if (currX < Board.WIDTH && currY < Board.HEIGHT)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>
	// <editor-fold desc="To Bottom-Left" defaultstate="collapsed">
	currX = originalLocation.x - moveXBy;
	currY = originalLocation.y + moveYBy;
	if (currX >= 0 && currY < Board.HEIGHT)
	{
	    Point p = new Point(currX, currY);
	    checkIfExists(p);
	}
	// </editor-fold>
	// <editor-fold desc="Up and Down" defaultstate="collapsed">
	currY = originalLocation.y - moveYBy;
	if (currY >= 0)
	{
	    Point p = new Point(originalLocation.x, currY);
	    checkIfExists(p);
	}
	currY = originalLocation.y + moveYBy;
	if (currY < Board.HEIGHT)
	{
	    Point p = new Point(originalLocation.x, currY);
	    checkIfExists(p);
	}
	// </editor-fold>
	// <editor-fold desc="Left and Right" defaultstate="collapsed">
	currX = originalLocation.x - moveXBy;
	if (currX >= 0)
	{
	    Point p = new Point(currX, originalLocation.y);
	    checkIfExists(p);
	}
	currX = originalLocation.x + moveXBy;
	if (currX < Board.WIDTH)
	{
	    Point p = new Point(currX, originalLocation.y);
	    checkIfExists(p);
	}
	// </editor-fold>
	return possibleLocations;
    }
}
