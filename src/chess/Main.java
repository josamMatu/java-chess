package chess;

import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 *
 * @author jotham
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
	// Setting the system's look and feel
	try
	{
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e)
	{
	}

	// Creating the chess board
	Board board = new Board();

	// Showing the chess board
	JFrame window = new JFrame("Chess");
	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	window.setResizable(false);
	window.getContentPane().add(board);
	window.pack();
	window.setLocationRelativeTo(null); // centering the window
	window.setVisible(true);

	// Starting the game
	board.start();
    }
}
