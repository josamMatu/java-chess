# README #

Hello.
This application was created using Java.

### What is this repository for? ###

* This repo contains 3 folders: "src" - containing the source code & images, "javadoc" - containing the source documentation, & "dist" - containing a compiled version of the project.

### How do I get set up? ###

* Before running the project, ensure that Java & the latest JDK are installed on your system.
* In case you encounter problems compiling or running the projects, revert to Java 7 & a corresponding JDK.

### Who do I talk to? ###

* In case of any queries you may reach me at: j.matu@yahoo.com.